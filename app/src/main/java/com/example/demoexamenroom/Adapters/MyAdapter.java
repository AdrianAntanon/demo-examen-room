package com.example.demoexamenroom.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demoexamenroom.Database.Gift;
import com.example.demoexamenroom.R;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<Gift> gifts;
    private final int layout;

    public MyAdapter(List<Gift> gifts, int layout) {
        this.gifts = gifts;
        this.layout = layout;
    }

    public void setGifts(List<Gift> gifts){
        this.gifts = gifts;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindGifts(this.gifts.get(position));
    }

    @Override
    public int getItemCount() {
        return this.gifts.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView itemViewGiftName, itemViewGiftPrice, itemViewGiftStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemViewGiftName = itemView.findViewById(R.id.textViewGift);
            this.itemViewGiftPrice = itemView.findViewById(R.id.textViewPrice);
            this.itemViewGiftStatus = itemView.findViewById(R.id.textViewGiftStatus);
        }

        public void bindGifts(Gift gifts) {
            this.itemViewGiftName.setText(gifts.getGiftName());

            boolean availableGift = gifts.isGiftStatus();
            String availableGiftText = "";
            if (availableGift){
                availableGiftText = "PENDIENTE";
            }else {
                availableGiftText = "COMPRADO";
            }

            this.itemViewGiftStatus.setText(availableGiftText);

            String giftPrice = gifts.getGiftPrice()+"€";

            this.itemViewGiftPrice.setText(giftPrice);

        }
    }
}