package com.example.demoexamenroom.Repositories;

import com.example.demoexamenroom.Database.DAOGift;
import com.example.demoexamenroom.Database.Gift;

import java.util.List;

public class GiftRepository {
    DAOGift daoGift;

    public GiftRepository(DAOGift daoGift) {
        this.daoGift = daoGift;
    }

    public void insertGift(Gift gift){
        daoGift.insertGift(gift);
    }

    public List<Gift> sortExpensiveGifts(){
        return daoGift.getExpensiveGifts();
    }

    public List<Gift> getAllGifts(){
        return daoGift.getAllGifts();
    }

    public int[] getAllPrices(){
        return daoGift.getGiftPrices();
    }

    public int getAmountOfGifts(){
        return daoGift.getAmountOfGits();
    }

    public void updateGifts(Gift gift){
        daoGift.updateGifts(gift);
    }

    public Gift findById(int id){
        return daoGift.findById(id);
    }

    public void deleteAllGifts(){
        daoGift.deleteAllGifts();
    }

}
