package com.example.demoexamenroom.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.demoexamenroom.Database.Gift;
import com.example.demoexamenroom.R;

public class AddGiftActivity extends AppCompatActivity {

    private boolean giftStatus;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_gift);

        EditText etGiftName = findViewById(R.id.editTextGiftName);
        EditText etGiftPrice = findViewById(R.id.editTextGiftPrice);

        Button addButton = findViewById(R.id.buttonAdd);

        Spinner spinner = findViewById(R.id.spinner);

        ArrayAdapter<CharSequence> arrayAdapter =ArrayAdapter.createFromResource(this, R.array.options, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int myPosition = spinner.getSelectedItemPosition();

//                Esto es IGUAL al if/else de más abajo, preguntar al profe porque no lo entiendo
                giftStatus = myPosition == 0;

//                if (myPosition == 0){
//                    giftStatus = true;
//                }else {
//                    giftStatus = false;
//                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        int [] listOfGiftPrices = MainActivity.repository.getAllPrices();

        int valueOfGifts = 0, maxGiftsValue = 200;

        for (int giftPrice:
                listOfGiftPrices) {
            valueOfGifts += giftPrice;
        }

        int finalValueOfGifts = valueOfGifts;

        addButton.setOnClickListener(v -> {
            if (etGiftName.getText().toString().isEmpty()){
                Toast.makeText(AddGiftActivity.this, "Es necesario rellenar todos los campos", Toast.LENGTH_SHORT).show();
            }else {
                if (etGiftPrice.getText().toString().isEmpty()){
                    Toast.makeText(AddGiftActivity.this, "Es necesario rellenar todos los campos", Toast.LENGTH_SHORT).show();
                }else {

                String giftName = etGiftName.getText().toString();

                String giftPriceString = etGiftPrice.getText().toString();
                int giftPrice = Integer.parseInt(giftPriceString);

                int currentGiftPrice = Integer.parseInt(giftPriceString);

                currentGiftPrice+= finalValueOfGifts;

                if (currentGiftPrice > maxGiftsValue){
                    Toast.makeText(AddGiftActivity.this, "El valor máximo de los regalos: "+maxGiftsValue+"€\n" +
                            "Saldo disponible: " + (maxGiftsValue - finalValueOfGifts) +"€", Toast.LENGTH_SHORT).show();
                }else {
                    Gift newGift = new Gift(giftName, giftPrice, giftStatus);

                    MainActivity.repository.insertGift(newGift);

                    Intent intent = new Intent(AddGiftActivity.this, MainActivity.class);

                    startActivity(intent);
                }



                }
            }
        });



    }
}
